all: clean
	Rscript -e "rmarkdown::render_site()"

clean:
	rm -rf public

preview:
	xdg-open public/index.html

p: preview
